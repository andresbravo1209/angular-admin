import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Router} from "@angular/router";

@Component({
  selector: 'app-secure',
  templateUrl: './secure.component.html',
  styleUrls: ['./secure.component.sass']
})
export class SecureComponent implements OnInit {

  user: any;

  constructor(
    private http: HttpClient,
    private router: Router) {

  }

  ngOnInit(): void {

    const headers = new HttpHeaders({
      Authorization: `Bearer ${localStorage.getItem('token')}`
    });

    this.http.get('http://angular-admin-back.test/user',{headers: headers}).subscribe(
      result => {
        this.user = result;
      },
        error => {
        localStorage.removeItem('token');
        this.router.navigate(['/login'])
      })
  }

}
