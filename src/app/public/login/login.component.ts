import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {

  form: FormGroup | any;

  constructor(private fb: FormBuilder, private http: HttpClient, private router: Router) { }

  ngOnInit(): void {

    this.form = this.fb.group({
      email: '',
      password: '',
    });

  }

  submit() {

    const formData = this.form.getRawValue();

    const data = {
      username: formData.email,
      password: formData.password,
      grant_type: 'password',
      client_id: 2,
      client_secret: 'FHAVGa18CT7qpnXD9K2RHKfWFL3YmR5hPJYnwzj4',
      scope: '*'
    };

    this.http.post('http://angular-admin-back.test/oauth/token', data)
      .subscribe(
        (result: any) => {
          localStorage.setItem('token', result.access_token);
          this.router.navigate(['/secure']);
        },
          error => {
            console.log('error')
            console.log(error.error)
        });

    console.log(this.form.getRawValue());
  }

}
